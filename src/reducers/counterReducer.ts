interface array {
}

interface CounterState {
    routes: array[];
    currentRoute: {};
}

const initialState: CounterState = {
    routes: [
        { name: "Маршрут 1", point1: 59.84660399 + ', ' + 30.29496392 , point2: 59.82934196 + ', ' + 30.42423701, point3: 59.83567701 + ', ' + 30.38064206 },
        { name: "Маршрут 2", point1: 59.82934196 + ', ' + 30.42423701, point2: 59.82761295 + ', ' + 30.41705607, point3: 59.84660399 + ', ' + 30.29496392},
        { name: "Маршрут 3", point1: 59.83567701 + ', ' + 30.38064206, point2: 59.84660399  + ', ' + 30.29496392, point3: 59.82761295 + ', ' + 30.41705607 },
    ],
    currentRoute: {},
};

const counterReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case 'ADD_ROUTE':
            return {...state, currentRoute: action.currentRoute};
        default:
            return state;
    }
};

export default counterReducer;