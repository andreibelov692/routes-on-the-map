export const addRoute = (currentRoute: string) => {
    return {
        type: 'ADD_ROUTE',
        currentRoute: currentRoute
    };
};