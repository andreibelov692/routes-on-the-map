import React, { useEffect, useRef } from 'react';
import { MapContainer, TileLayer, Marker } from 'react-leaflet';
import L from 'leaflet';
import 'leaflet-routing-machine';
import 'leaflet/dist/leaflet.css';
import 'leaflet-routing-machine/dist/leaflet-routing-machine.css';

import markerIcon from 'leaflet/dist/images/marker-icon.png';
import markerIconRetina from 'leaflet/dist/images/marker-icon-2x.png';
import markerShadow from 'leaflet/dist/images/marker-shadow.png';
import { useDispatch, useSelector } from 'react-redux';
import { addRoute } from '../../actions';

delete (L.Icon.Default.prototype as any)._getIconUrl;
L.Icon.Default.mergeOptions({
    iconRetinaUrl: markerIconRetina,
    iconUrl: markerIcon,
    shadowUrl: markerShadow,
});

const getMidpoint = (point1: number[], point2: number[]) => {
    const lat = (point1[0] + point2[0]) / 2;
    const lng = (point1[1] + point2[1]) / 2;
    return [lat, lng];
};

const Map: React.FC = () => {
    const mapRef = useRef<L.Map | null>(null);
    const routingControlRef = useRef<any>(null); // Ссылка на объект маршрута
    const routes = useSelector((state: any) => state.routes);
    const currentRoute = useSelector((state: any) => state.currentRoute);
    const currentRoutePoint1 = useSelector((state: any) =>
        parseFloat(state.currentRoute.point1)
    );
    const currentRoutePoint2 = useSelector((state: any) =>
        parseFloat(state.currentRoute.point2)
    );
    const currentRoutePoint3 = useSelector((state: any) =>
        parseFloat(state.currentRoute.point3)
    );
    const dispatch = useDispatch();

    const handleIncrement = () => {
        const routeName = 'Маршрут 1';
        const foundRoute = routes.find((route: any) => route.name === routeName);
        if (foundRoute) {
            dispatch(addRoute(foundRoute));
        } else {
            console.log('маршрут не найден');
        }
    };
    const points = [
        [currentRoutePoint1, 30.29496392],
        [currentRoutePoint2, 30.42423701],
        [currentRoutePoint3, 30.38064206]
    ];
    useEffect(() => {
        handleIncrement();
    }, []);

    useEffect(() => {
        if (mapRef.current) {
            const map = mapRef.current;

            if (routingControlRef.current) {
                routingControlRef.current.remove();
            }

            const points = [
                [currentRoutePoint1, 30.29496392],
                [currentRoutePoint2, 30.42423701],
                [currentRoutePoint3, 30.38064206]
            ];

            if (points.length > 1) {
                const waypoints = points.map((point) => L.latLng(point[0], point[1]));

                routingControlRef.current = L.Routing.control({
                    waypoints: waypoints,
                    lineOptions: {
                        styles: [{ color: 'blue', opacity: 0.6, weight: 4 }],
                        extendToWaypoints: true,
                        missingRouteTolerance: 100,
                    },
                    addWaypoints: false,
                    routeWhileDragging: true,
                    router: new L.Routing.OSRMv1({ serviceUrl: 'http://router.project-osrm.org/route/v1' }), // Здесь указывается URL сервиса OSRM
                }).addTo(map);
            }
        }
    }, [currentRoutePoint1, currentRoutePoint2, currentRoutePoint3, routes]);

    useEffect(() => {

        if (mapRef.current && points.length > 1) {
            const map = mapRef.current;
            const midpoint = getMidpoint(points[0], points[points.length - 1]);
            // @ts-ignore
            map.setView(midpoint, 12);
        }
    }, [points]);

    return (
        <MapContainer center={[59.82934196, 30.42423701]}  style={{ height: '100%', width: '100%' }} ref={mapRef}>
            <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />

        </MapContainer>
    );
};

export default Map;