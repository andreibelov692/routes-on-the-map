import React from 'react';
import { useDispatch, useSelector } from "react-redux";
import { addRoute } from "../../actions";
import s from './Routes.module.css'
interface Route {
    name: string;
    point1: number;
    point2: number;
    point3: number;
}

interface RootState {
    routes: Route[];
    currentRoute: Route | null;
}

interface TableProps {}

const Table: React.FC<TableProps> = () => {
    const routes = useSelector((state: RootState) => state.routes);
    const dispatch = useDispatch();

    const handleIncrement = (routeName: string) => {
        const foundRoute = routes.find(route => route.name === routeName);
        if (foundRoute) {
            // @ts-ignore
            dispatch(addRoute(foundRoute));
        } else {
            console.log('маршрут не найден');
        }
    };

    return (
        <table className={s.table}>
            <thead>
            <tr>
                <th>Маршрут</th>
                <th>Точка 1</th>
                <th>Точка 2</th>
                <th>Точка 3</th>
            </tr>
            </thead>
            <tbody>
            {routes.map((route: Route, index: number) => (
                <tr key={index} onClick={() => handleIncrement(route.name)}>
                    <td>{route.name}</td>
                    <td>{route.point1}</td>
                    <td>{route.point2}</td>
                    <td>{route.point3}</td>
                </tr>
            ))}
            </tbody>
        </table>
    );
};

export default Table;