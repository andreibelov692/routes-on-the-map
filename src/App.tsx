import React from 'react';

import './App.css';
import Map from "./Components/Map/Map";
import Table from "./Components/Routes/Routes";

function App() {
  return (
    <div className="App">
        <Table></Table>
      <div className="map">
        <Map></Map>
      </div>
    </div>
  );
}

export default App;
